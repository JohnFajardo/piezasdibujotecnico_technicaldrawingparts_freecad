# Piezas Dibujo Técnico / FreeCAD

Una biblioteca de piezas de código abierto para uso de material didáctico en las escuelas, en el proceso de enseñanza/aprendizaje del dibujo de vistas ortogonales y perspectivas.
Todas las piezas son imprimibles en 3D, y tienen un volumen estándar de 40mmx40mmx40mm para tener un equilibrio entre economía de material y tiempo de impresión.
En el caso de no tener una impresora 3D, se pueden descargar los diseños de las piezas con sus caras desplegadas en 2D para poder imprimir en una hoja A4, y asi recortar, pegar y armar.

# Archivos

Cada pieza tiene siete (7) archivos

- .fcstd (FreeCAD estandar)
- .jpg   (vista isométrica acotada para dibujar)
- .obj 	 (para impresión 3D)
- .pdf	 (para impresión 2D)
- .png   (imagen de visualización)
- .stl 	 (para impresión 3D)
- .svg   (edición despliegue 2D)

# Piezas

Algunas piezas primitivas para trabajar en el aula.

- 000_Primitivas-Primitives:

<img src="primitivas.png" width="400" align="center">



Piezas organizadas en grupos por nivel de dificultad.

- 001_Fácil-Easy:

<img src="faciles.png" width="600" align="center">



- 010_Medio-Medium:

<img src="medias.png" width="600" align="center">



- 011_Difícil-Hard:

<img src="dificiles.png" width="600" align="center">



# Otras piezas

Fuera del volumen estandar (40x40x40)
Para las practicas de vistas, cortes, perspectivas, escalas, acotaciones y planos.

- 100_Mezcla-Mixture:

Cargando...

- 101_Mecánica-Mechanics:

Cargando...


# Licencia:

Los diseños de  PiezasDibujoTecnico_TechnicalDrawingParts_FreeCAD estan bajo licencia [Creative Commons Atribución-CompartirIgual 4.0 Internacional](http://creativecommons.org/licenses/by-sa/4.0/)

<img src="By-sa.png" width="100" align="center">


# Contribuir

Por favor, no dude en contribuir a esta biblioteca.

----------------------------------------------------------------------------------------------------------------

# Technical Drawing Parts / FreeCAD

A library of open source parts for use of teaching materials in schools, in the process of teaching / learning the drawing of orthogonal views and perspectives.
All parts are printable in 3D, and have a standard volume of 40mmx40mmx40mm to have a balance between material economy and printing time.
In the case of not having a 3D printer, you can download the designs of the parts with their faces displayed in 2D to be able to print on an A4 sheet, and so cut, paste and assemble.

# Files

Each part has seven (7) files.

- .fcstd (FreeCAD standar)
- .jpg   (isometric view dimensioned to draw)
- .obj 	 (for 3D printing)
- .pdf	 (for 2D printing)
- .png   (visualization image)
- .stl 	 (for 3D printing)
- .svg   (2D deployment edition)

# Parts

Some primitive parts to work in the classroom.

- 000_Primitivas-Primitives:

<img src="primitivas.png" width="400" align="center">



Parts organized in groups by level of difficulty.

- 001_Fácil-Easy:



<img src="faciles.png" width="600" align="center">

- 010_Medio-Medium:

<img src="medias.png" width="600" align="center">



- 011_Difícil-Hard:

<img src="dificiles.png" width="600" align="center">



# Other parts

Out of the standard volume (40x40x40)
For the practices of views, cuts, perspectives, scales, dimensions and plans.

- 100_Mezcla-Mixture:

Loading...

- 101_Mecánica-Mechanics:

Loading...


# License:

The designs  PiezasDibujoTecnico_TechnicalDrawingParts_FreeCAD are licensed under [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)

<img src="By-sa.png" width="100" align="center">


# Contribute

Please, do not hesitate on contributing to this library.

